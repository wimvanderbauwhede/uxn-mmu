# Uxn-MMU

An assembler and emulator for the [Uxn stack-machine](https://wiki.xxiivv.com/site/uxn.html), written in ANSI C.

The version in this repo has a memory management unit (MMU) which adds support for virtual memory with dynamic memory allocation. See [DESIGN.md](https://codeberg.org/wimvanderbauwhede/uxn-mmu/src/branch/main/DESIGN.md) for more details.

## Enabling dynamic memory allocation in your program

I use the `System/expansion` slot for this functionality, `allocate` and `deallocate` use the codes `0x02` and `0x03` respectively. So you need the `System` device:

	|0000
	|00 @System [ &unused $2 &expansion $2 &wst $1 &rst $1 &meta $2 &r $2 &g $2 &b $2 &dbg $1 &state $1 ]
	@ptr $2

 You can define some convenience functions to `allocate` and `deallocate` memory:

	@allocate ( : Uint16 -> Uint16 )
		#02 .System/expansion DEO2
	JMP2r

	@deallocate ( : Uint16 -> None )
		#03 .System/expansion DEO2
	JMP2r

With those, when you allocate a given amount of memory, it returns a memory pointer that can be used by `LDA` and `STA` just like any other memory address. 

	|0100
		#0400 allocate ( returns a 2-byte virtual memory pointer )
		DUP2 #2a2a SWP2 STA2
		DUP2 LDA2
		deallocate ( takes a 2-byte virtual memory pointer )
	BRK

In the current design, the address range above 48K is set aside for the virtual memory, and the memory pages are fixed at 64 bytes.
If allocation failed, `allocate` returns 0. But `LDA` and `STA` do not check if the pointer passed is valid. They also do not check if you access memory outside of the allocated range.

## Build

To enable the MMU, build Uxn with the `--mmu` option.

### Linux/OS X

To build the Uxn emulator, you must install [SDL2](https://wiki.libsdl.org/) for your distro. If you are using a package manager:

```sh
sudo pacman -Sy sdl2             # Arch
sudo apt install libsdl2-dev     # Ubuntu
sudo xbps-install SDL2-devel     # Void Linux
brew install sdl2                # OS X
```

Build the assembler and emulator by running the `build.sh` script. The assembler(`uxnasm`) and emulator(`uxnemu`) are created in the `./bin` folder.

```sh
./build.sh 
	--debug # Add debug flags to compiler
	--format # Format source code
	--install # Copy to ~/bin
	--mmu # enable MMU support, for dynamic allocation
```

If you wish to build the emulator without graphics mode:

```sh
cc src/devices/datetime.c src/devices/system.c src/devices/console.c src/devices/file.c src/uxn.c -DNDEBUG -Os -g0 -s src/uxncli.c -o bin/uxncli
```

### Plan 9 

To build and install the Uxn emulator on [9front](http://9front.org/), via [npe](https://git.sr.ht/~ft/npe):

```rc
mk install
```

If the build fails on 9front because of missing headers or functions, try again after `rm -r /sys/include/npe`.

### Windows

Uxn can be built on Windows with [MSYS2](https://www.msys2.org/). Install by downloading from their website or with Chocolatey with `choco install msys2`. In the MSYS shell, type:

```sh
pacman -S git mingw-w64-x86_64-gcc mingw64/mingw-w64-x86_64-SDL2
export PATH="${PATH}:/mingw64/bin"
git clone https://git.sr.ht/~rabbits/uxn
cd uxn
./build.sh
```

If you'd like to work with the Console device in `uxnemu.exe`, run `./build.sh --console` instead: this will bring up an extra window for console I/O unless you run `uxnemu.exe` in Command Prompt or PowerShell.

## Getting Started

### Emulator

To launch a `.rom` in the emulator, point the emulator to the target rom file:

```sh
bin/uxnemu bin/piano.rom
```

You can also use the emulator without graphics by using `uxncli`. You can find additional roms [here](https://sr.ht/~rabbits/uxn/sources), you can find prebuilt rom files [here](https://itch.io/c/248074/uxn-roms). 

### Assembler 

The following command will create an Uxn-compatible rom from an [uxntal file](https://wiki.xxiivv.com/site/uxntal.html). Point the assembler to a `.tal` file, followed by and the rom name:

```sh
bin/uxnasm projects/examples/demos/life.tal bin/life.rom
```

### I/O

You can send events from Uxn to another application, or another instance of uxn, with the Unix pipe. For a companion application that translates notes data into midi, see the [shim](https://git.sr.ht/~rabbits/shim).

```sh
uxnemu orca.rom | shim
```

## GUI Emulator Options

- `-2x` Force medium scale
- `-3x` Force large scale

## GUI Emulator Controls

- `F1` toggle zoom
- `F2` toggle debug
- `F3` capture screen
- `F4` reboot
- `F5` soft reboot

### GUI Buttons

- `LCTRL` A
- `LALT` B
- `LSHIFT` SEL 
- `HOME` START

## Need a hand?

The following resources are a good place to start:

* [XXIIVV — uxntal](https://wiki.xxiivv.com/site/uxntal.html)
* [XXIIVV — uxntal reference](https://wiki.xxiivv.com/site/uxntal_reference.html)
* [compudanzas — uxn tutorial](https://compudanzas.net/uxn_tutorial.html)
* [Fediverse — #uxn tag](https://merveilles.town/tags/uxn)

## Contributing

Submit patches using [`git send-email`](https://git-send-email.io/) to the [~rabbits/public-inbox mailing list](https://lists.sr.ht/~rabbits/public-inbox).
