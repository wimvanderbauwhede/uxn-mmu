# Uxn Memory Management Unit and Virtual Memory

A memory management unit manages memory by dividing it in small chunks called pages. It keeps track of which pages are being used, and presents the user with a contiguous memory range. The aim is that as a user, this "virtual" memory can be used just like real memory.

This is a design for a simple Memory Management Unit (MMU) in Uxn, accessed via a system device.

- Set aside the higher 16K for dynamic allocation.
- Allocate pages of 32 2-byte words

So we have 256 such pages. For the free pages, we can keep a stack `freePages` of 256 pages, i.e. an array of bytes with a stack pointer `freePagePtr`.

    unsigned char freePagesStack[256];
    unsigned char freePagePtr;

How to track the allocated pages? The worst case is 256 unique uses. The best case is a single use of all the memory.
We use an array `usedPages` of 256 bytes which has the used pages, and an auxiliary array `allocMap` which stores the start and length of a sequence, and its status. The virtual address reference would be the index into that array, but more on that later. The `allocMap` is 256 (startIdx, length, status) tuples. We also have an index `usedPageIdx` and `allocMapIdx`.


    typedef struct __attribute__((packed)) PageAllocTup {
        unsigned char startIdx:8; /* 8 bits */
        unsigned char length:6; /* 6 bits */
        unsigned char status:2; /* 2 bits */
    } PageAllocTup; /* must be packed */

    unsigned char usedPages[256];
    PageAllocTup allocMap[256];
    unsigned char usedPageIdx, allocMapIdx;


The interface is quite simple, we have `allocate` and `deallocate`:

    allocate :: Int -> Int -- the return value is some reference to the allocated memory. 0 means failure.

or in C syntax:

    unsigned char allocate(unsigned char);

What this does is:

- get usedPageIdx
- write the pages from the stack into this location at usedPages;
- increment usedPageIdx
- write the tuple into allocMap at allocMapIdx
- return allocMapIdx
- increment allocMapIdx

For deallocation:

    deallocate :: Int -> Int -- takes the reference and frees up the memory

or in C:

    unsigned char free(unsigned char);

This pushes the sequence of pages for a reference back on the `freePages` stack.

- from `allocMap` we get startIdx and length
- we push the pages on the stack and write a 0 for each page in `usedPages`
- we set the status to 0

At this point we have not freed up space in `usedPages`

compact :: Int -- returns the space saved through compaction; this could be part of allocate but it is slower.

    unsigned char compact();

We need to remove freed references.

- iterate through allocMap
- in usedPages, for a freed entry of n pages at position idx in allocMap, move every element at idx+n+j to idx+j
- subtract n from every startIdx > idx in allocMap
- set the status of the compacted entry to unused (we have: unused, allocated, freed; if unused, it can be allocated.)
- repeat this until there are no freed entries.

Finally, we need a function that will take a contiguous index into the allocated sequence, and work out the actual page and index in that page.
Basically, this is like STA2 and LDA2.

What would be ideal is if the alloc would return a proper virtual address >48K. I think that might be possible. Then we could simply use STA and LDA.
What this would take is that we treat the sum of the lengths in allocMap as a contiguous offset.

    ( addr - 48K ) % 64

The problem is that if we allocate a virtual pointer like that, then it is impossible to ensure that a fresh virtual pointer will not overlap with the range of this already allocated virtual pointer. The only way to handle this is by not compacting, but that leaves a lot less scope as we can only fill the holes.

"Filling the holes" means we check the size of a pending block. If it is larger than the size to allocate, we also need to insert a new pending block with the remainder of the size.

## Possible allocation strategies

In any case, we need

- set page size
- set dyn alloc address range start address (aka virtual address range start address)
- allocate
- deallocate

Depending on the strategy, we might also need compact or consolidate; and vload/vstore.

### 1. Most space and time efficient

The `allocate` call returns a 1-byte reference and a 1-byte status, which is the number of remaining free pages. This is wasteful but we need to make sure the allocation succeeded or not.
The `deallocate` call takes a 1-byte reference. This call will always succeed, but we might return a byte indicating if `compact` should be called or not.

We compact the blocks and adapt the start addresses of the references. The references themselves don't change. But we don't have a proper virtual address: we have an opaque reference and a virtual address space attached to it.
So we have something like

    #0133 #1e #01 vload
    #0123 #0133 #1e #02 vstore

(and with a macro

    VLD { #01 vload },
    VST2 { #02 vstore }

etc. it looks a little better.)

And these would be implemented using the expansion slot.
Most time effective is to only compact when the alloc has failed, rather than on every alloc as we do currently.

In addition to `allocate` and `deallocate` we also nee `grow` and `shrink`.

- `grow` makes a hole by shifting pages up, after checking there is still space to do so;
- `shrink` is like a partial `deallocate`.

With this approach, there is no reason to reserve the top 16KB. We could create an additional memory of 64K in pages of 256 bytes instead.

### 2. Most transparent

The `allocate` call returns a 2-byte virtual address. If it fails we can return 0.
The `deallocate` call takes a 2-byte virtual address. This call will always succeed.

We don't compact; instead, de look for pending blocks of sufficient size, see above. This wastes both space and time, but it means we get proper addresses, so can simply modify STA and LDA so that any address in the virtual address space is handled properly. We have an additional consolidate action, which will consolidate contiguous pending blocks. The difference between pending and free is that a deallocate at the top moves the usedPagesIdx back and so any record above that index is properly free. When we set a record to free, it means it has not block attached to it.

    #1f33 LDA
    #0123 #1f33 STA2










