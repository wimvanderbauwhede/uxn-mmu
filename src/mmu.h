
/* The idea is to create an MMU and access it via a system device. In principle we can of course implement this in native Uxntal as well.

- Suppose I set aside the higher 16K for dynamic allocation.
- Suppose we allocate pages of 32 2-byte words
Then we have 256 such pages.
*/

#ifndef __MMU_H__
#define __MMU_H__
/* Maybe putting these under programmer control is better */

/* #define TRANSPARENT_VMEM */
#define VMEM_START_ADDR 48*1024
#define VMEM_START_PAGE 768
#define VMEM_PAGE_SZ 64 /* bytes */
#define VMEM_PAGE_MASK 0x3f
#define VMEM_PAGE_BITS 6

typedef struct __attribute__((packed)) PageAllocTup {
    unsigned char startIdx:8; /* 8 bits */
    unsigned char length:6; /* 6 bits */
    unsigned char status:2; /* 2 bits */
} PageAllocTup; /* must be packed */

typedef struct VMem {

    unsigned char freePagesStack[256];
    signed int freePagePtr;

    unsigned char usedPages[256];
    unsigned char usedPageIdx;

    PageAllocTup allocMap[256];
    unsigned char allocMapIdx;

} VMem;

void initVMem(VMem* vmem);
#ifdef TRANSPARENT_VMEM
unsigned short allocate(unsigned short, VMem* vmem);
void deallocate(unsigned short, VMem* vmem);
void consolidate(VMem* vmem);
unsigned char vread(unsigned short, unsigned char*, VMem* vmem);
void vwrite(unsigned short,unsigned char, unsigned char*, VMem* vmem);
unsigned short vread2(unsigned short, unsigned char*, VMem* vmem);
void vwrite2(unsigned short,unsigned short, unsigned char*, VMem* vmem);
#else
unsigned char allocate(unsigned short, VMem* vmem);
void deallocate(unsigned char, unsigned short, VMem* vmem);
void compact(VMem* vmem);
unsigned char vread(unsigned char, unsigned short, unsigned char*, VMem* vmem);
void vwrite(unsigned char, unsigned short,unsigned char, unsigned char*, VMem* vmem);
unsigned short vread2(unsigned char, unsigned short, unsigned char*, VMem* vmem);
void vwrite2(unsigned char, unsigned short,unsigned short, unsigned char*, VMem* vmem);
#endif
#endif
