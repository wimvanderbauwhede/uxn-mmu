/*
The system device at the moment has  00 and 01 as unused; 02 and 03 as expansion;
04 and 05 are wst and rst

expansion is an operation, so I think I might be able to use this.

@on-reset ( -> )
	;cmd .System/expansion DEO2
	;dst print-str
	BRK

@cmd [ 01 000b 0000 =src 0000 =dst ]
@src "Hello 20 "World $1
@dst $c

The line in system_deo() (in system.c)

    if(ram[addr] == 0x1) { ... }

is the operation.

This would work to implement just about everything for the mmu.
The API is trivial 

#1234 allocate, takes the nbytes of the stack, leaves the pointer (or 0) on the stack

#1234 deallocate,  takes the pointer off the stack

@allocate ( : Uint16 -> Uint16 )
    #02 .System/expansion DEO2
JMP2r

@deallocate ( : Uint16 -> None )
    #03 .System/expansion DEO2
JMP2r

*/
#include <stdlib.h>
#include <stdio.h>
/* #include <string.h>  */ /* maybe needed for memcpy */
#include "mmu.h"

#define FREE 0
#define PENDING 1
#define ALLOCATED 2

void initVMem(VMem* vmem) {
    unsigned short ii;
    for (ii=0;ii<256;++ii) {
        vmem->freePagesStack[ii]=ii;
        vmem->usedPages[ii]=0;
        /* freePagePtr=ii; */ /* This can only be 0 .. 255 so it will have to be pointer to the top used position, not the one above it */
        vmem->allocMap[ii].startIdx=0;
        vmem->allocMap[ii].length=0;
        vmem->allocMap[ii].status=FREE; /* a page can be used, free or pending */
    }
    vmem->freePagePtr=255;
    vmem->usedPageIdx=0;
    vmem->allocMapIdx=0;

}

#ifdef TRANSPARENT_VMEM
/*
The interface is quite simple:
allocate :: Uint16 -> Uint16 -- the return value is some reference to the allocated memory. 0 means failure.
*/
unsigned short allocate(unsigned short nbytes, VMem* vmem) {
    /* printf("vmem->freePagePtr:%u\n",vmem->freePagePtr); */
    /* Rounding up */
    unsigned short npages = nbytes >> VMEM_PAGE_BITS ;
    if (npages * VMEM_PAGE_SZ < nbytes ) {
        npages++;
    }
    /*
    - get usedPageIdx
    - write the pages from the stack into this location at usedPages;
    */
   /* First we need to check if there are enough pages left. */
    /* if (freePagePtr==0) { return 0; } */
    if (vmem->freePagePtr<npages) {
#ifdef DBG
        printf("Fail: not enough free pages\n");
#endif
            return 0;
    }

   /* if we use compaction, this should always work;
   but otherwise, we need to check if the range at the top is high enough
   */
  /* printf("usedPageIdx=%u;usedPageIdx+npages=%u\n",usedPageIdx,usedPageIdx+npages); */
    if (vmem->usedPageIdx+npages<256) {
        unsigned short ii;
        for (ii=vmem->usedPageIdx;ii<vmem->usedPageIdx+npages;++ii) {
            vmem->usedPages[ii]=vmem->freePagesStack[vmem->freePagePtr--];
        }
#ifdef DBG
        printf("Allocated %u pages, free page ptr: %d, allocRef: %u\n",npages, vmem->freePagePtr,vmem->allocMapIdx);
#endif
/* - write the tuple into allocMap at allocMapIdx */
        vmem->allocMap[vmem->allocMapIdx].startIdx=vmem->usedPageIdx;
        vmem->allocMap[vmem->allocMapIdx].length=npages;
        vmem->allocMap[vmem->allocMapIdx].status=ALLOCATED; /* a page can be used, free or pending */
/* - increment usedPageIdx */
        vmem->usedPageIdx += npages;
        ++vmem->allocMapIdx;
        return ((vmem->usedPageIdx-npages)<<VMEM_PAGE_BITS)+VMEM_START_ADDR;
    } else {
        unsigned short jj;
        unsigned short allocOffset=0;
        for (jj=0;jj<vmem->allocMapIdx;++jj) {
            if (vmem->allocMap[jj].status==PENDING && vmem->allocMap[vmem->allocMapIdx].length<=npages) {
#ifdef DBG
                printf("Reallocate %u pages, free page ptr: %d\n",npages, vmem->freePagePtr);
#endif
                vmem->allocMap[jj].status=ALLOCATED;
                vmem->allocMap[vmem->allocMapIdx].length=npages;
                unsigned short ii;
                for (ii=jj;ii<jj+npages;++ii) {
                    vmem->usedPages[ii]=vmem->freePagesStack[vmem->freePagePtr--];
                }
#ifdef DBG
                printf("After realloc: free page ptr: %d\n", vmem->freePagePtr);
#endif
                allocOffset=jj;
                break;
            }
        }
        if (allocOffset==0) {
#ifdef DBG
            printf("Fail: no suitable hole\n");
#endif
            return 0; /* Failed */
        } else {
            return VMEM_START_ADDR + (allocOffset<<VMEM_PAGE_BITS);
        }
    }

} /* END of allocate */
#else
/*
The interface is quite simple:
allocate :: Uint16 -> Uint8 -- the return value is some reference to the allocated memory. 0 means failure.
*/
unsigned char allocate(unsigned short nbytes, VMem* vmem) {
    /* printf("vmem->freePagePtr:%u\n",vmem->freePagePtr); */
    /* Rounding up */
    unsigned short npages = nbytes >> VMEM_PAGE_BITS ;
    if (npages * VMEM_PAGE_SZ < nbytes ) {
        npages++;
    }
    /*
    - get usedPageIdx
    - write the pages from the stack into this location at usedPages;
    */
   /* First we need to check if there are enough pages left. */
    /* if (freePagePtr==0) { return 0; } */
    if (vmem->freePagePtr<npages) {
#ifdef DBG
        printf("Fail: not enough free pages\n");
#endif
            return 0;
    }

    unsigned short ii;
    for (ii=vmem->usedPageIdx;ii<vmem->usedPageIdx+npages;++ii) {
        vmem->usedPages[ii]=vmem->freePagesStack[vmem->freePagePtr--];
    }
#ifdef DBG
    printf("Allocated %u pages, free page ptr: %d, allocRef: %u\n",npages, vmem->freePagePtr,vmem->allocMapIdx);
#endif
/* - write the tuple into allocMap at allocMapIdx */
    vmem->allocMap[vmem->allocMapIdx].startIdx=vmem->usedPageIdx;
    vmem->allocMap[vmem->allocMapIdx].length=npages;
    vmem->allocMap[vmem->allocMapIdx].status=ALLOCATED; /* a page can be used, free or pending */
/* - increment usedPageIdx */
    vmem->usedPageIdx += npages;
    ++vmem->allocMapIdx;
    return vmem->allocMapIdx-1;

} /* END of allocate */
#endif

#ifdef TRANSPARENT_VMEM
/*
deallocate :: Int -> Int -- takes the reference and frees up the memory
*/
void deallocate(unsigned short addrRef, VMem* vmem) {
    /* printf("vmem->freePagePtr:%u;addrRef:%u\n",vmem->freePagePtr,addrRef); */
    unsigned short allocRef=257;
    unsigned short ii;
#ifdef TRANSPARENT_VMEM
    if (addrRef<VMEM_START_ADDR) {
        printf("Address %u is not a virtual address, should be > %u\n",addrRef, VMEM_START_ADDR);
        exit(-1);
    }
    unsigned short refOffset = (addrRef - VMEM_START_ADDR)>>VMEM_PAGE_BITS;
#ifdef DBG
    printf("%u;%u\n",addrRef ,refOffset);
#endif
    /* The ref is the one that has the offset */
    for ( ii=0;ii<256;++ii ) {
        if (vmem->allocMap[ii].status !=FREE && vmem->allocMap[ii].startIdx == refOffset) {
#ifdef DBG
            printf("Deallocating ref %u\n",ii);
#endif
            allocRef = ii;
            break;
        }
    }
#else
    allocRef = addrRef;
#endif
    if (allocRef>256 || vmem->allocMap[allocRef].status!=ALLOCATED) {
        printf("Reference %u has already been freed, status is %u\n",allocRef, vmem->allocMap[allocRef].status);
        exit(-1);
    }
    unsigned char startIdx = vmem->allocMap[allocRef].startIdx;
    unsigned char npages = vmem->allocMap[allocRef].length;
/*
This pushes the sequence of pages for a reference back on the freePages stack.
- from allocMap we get startIdx and length
- we push the pages on the stack
- and write a 0 for each page in usedPages => no use as 0 is a valid page
*/
#ifdef DBG
    printf("Pushing %u pages back onto the stack\n",npages);
#endif
    for (ii=startIdx;ii<startIdx+npages;++ii) {
        vmem->freePagesStack[++vmem->freePagePtr]=vmem->usedPages[ii];
    }
#ifdef DBG
    printf("Page stack ptr: %d\n",vmem->freePagePtr);
#endif
/*
- we set the status to 1 (pending)
*/
    vmem->allocMap[allocRef].status=PENDING;
#ifdef TRANSPARENT_VMEM
    consolidate(vmem);
#else
    compact(vmem);
#endif
/* The obvious problem is that we have not freed up space in usedPages */
} /* END of deallocate */
#else
/*
deallocate :: Int -> Int -- takes the reference and frees up the memory
*/
void deallocate(unsigned char allocRef, unsigned short nbytes,VMem* vmem) {
    #error "TODO use nbytes grow shrink"
    /* printf("vmem->freePagePtr:%u;allocRef:%u\n",vmem->freePagePtr,allocRef); */
    unsigned short ii;

    if ( vmem->allocMap[allocRef].status!=ALLOCATED) {
        printf("Reference %u has already been freed, status is %u\n",allocRef, vmem->allocMap[allocRef].status);
        exit(-1);
    }
    unsigned char startIdx = vmem->allocMap[allocRef].startIdx;
    unsigned char npages = vmem->allocMap[allocRef].length;
/*
This pushes the sequence of pages for a reference back on the freePages stack.
- from allocMap we get startIdx and length
- we push the pages on the stack
- and write a 0 for each page in usedPages => no use as 0 is a valid page
*/
#ifdef DBG
    printf("Pushing %u pages back onto the stack\n",npages);
#endif
    for (ii=startIdx;ii<startIdx+npages;++ii) {
        vmem->freePagesStack[++vmem->freePagePtr]=vmem->usedPages[ii];
    }
#ifdef DBG
    printf("Page stack ptr: %d\n",vmem->freePagePtr);
#endif
/*
- we set the status to 1 (pending)
*/
    vmem->allocMap[allocRef].status=PENDING;
    compact(vmem);

} /* END of deallocate */
#endif


#ifdef TRANSPARENT_VMEM
/*
We need to go through all allocRefs and find contiguous ones
So this is quadratic;
 */
void consolidate(VMem* vmem) {
    unsigned short allocRef;
    for (allocRef=0;allocRef<256;++allocRef) {
        unsigned short callocRef;
        for (callocRef=0;callocRef<256;++callocRef) {
            if (vmem->allocMap[allocRef].startIdx+vmem->allocMap[allocRef].length==vmem->allocMap[callocRef].startIdx
            && vmem->allocMap[callocRef].status==PENDING && vmem->allocMap[allocRef].status!=ALLOCATED
            && vmem->allocMap[allocRef].length>0) {
#ifdef DBG
                printf("Consolidating %u and %u\n",allocRef,callocRef);
#endif
                vmem->allocMap[allocRef].length+=vmem->allocMap[callocRef].length;
                vmem->allocMap[callocRef].status=FREE;
            }
        }
    }
} /* END of consolidate */
#else
/*
compact :: Int -- returns the space saved through compaction; this could be part of allocate but it is slower.
*/
void compact(VMem* vmem) {
/*
We need to remove freed references.
- iterate through allocMap
*/
    unsigned char has_pending_blocks=1;
    while(has_pending_blocks) {
        unsigned short allocRef;
        for (allocRef=0;allocRef<256;++allocRef) {
            has_pending_blocks=0;
            if (vmem->allocMap[allocRef].status==PENDING) {
                has_pending_blocks=1;
                unsigned char startIdx = vmem->allocMap[allocRef].startIdx;
                unsigned char npages = vmem->allocMap[allocRef].length;
/*
- in usedPages, for a pending entry of n pages at position idx in allocMap, move every element at idx+n+j to idx+j
*/
                unsigned short entry;
                for (entry=startIdx;entry<startIdx+npages;++entry) {
                    vmem->usedPages[startIdx+entry] = vmem->usedPages[startIdx+npages+entry];
                }
/*
- subtract n from every startIdx > idx in allocMap
*/
                unsigned short jj;
                for (jj=startIdx+1;jj<vmem->allocMapIdx;++jj) {
                    vmem->allocMap[jj].startIdx-=npages;
                }
    /*
- set the status of the compacted entry to free (we have: free, allocated, pending)
*/
                vmem->allocMap[allocRef].status=FREE;
                vmem->usedPageIdx-=npages;
            } /* if */
        } /* for */
/*
- repeat this until there are no pending entries.
*/
    } /* while */
} /* END of compact */
#endif


/*
Finally, we need a function that will take a contiguous index into the allocated sequence, and work out the actual page and index in that page.
Basically, this is like STA2 and LDA2.

What would be ideal is if the alloc would return a proper virtual address >48K. I think that might be possible. Then we could simply use STA and LDA.
What this would take is that we treat the sum of the lengths in allocMap as a contiguous offset.


unsigned char virtAddrToRef(unsigned short addr) {
( addr - 48K ) % 64

It does mean we need to iterate trough the allocMap. We can speed this up a bit by having another map which has reverse (offset, idx) pairs, revAllocMap


*/
#ifdef TRANSPARENT_VMEM
unsigned char vread(unsigned short addrRef, unsigned char* ram, VMem* vmem) {
#ifdef DBG
    printf("vread\n");
#endif
    if (addrRef<VMEM_START_ADDR) {
        return ram[addrRef];
    } else {
        unsigned short refOffset = (addrRef - VMEM_START_ADDR)>>VMEM_PAGE_BITS;
        unsigned short inPageIdx = addrRef & VMEM_PAGE_MASK;

        unsigned short pageIdx = vmem->usedPages[refOffset];
        unsigned short addr = (pageIdx<<VMEM_PAGE_BITS)+inPageIdx;
        return ram[addr];
    }
}

void vwrite(unsigned short addrRef,unsigned char val, unsigned char* ram, VMem* vmem) {
#ifdef DBG
    printf("vwrite\n");
#endif
    if (addrRef<VMEM_START_ADDR) {
        ram[addrRef]=val;
    } else {
        unsigned short refOffset = (addrRef - VMEM_START_ADDR)>>VMEM_PAGE_BITS;
        unsigned short inPageIdx = addrRef & VMEM_PAGE_MASK;

        unsigned short pageIdx = vmem->usedPages[refOffset];
        unsigned short addr = (pageIdx<<VMEM_PAGE_BITS)+inPageIdx;
        ram[addr] = val;
    }

}

unsigned short vread2(unsigned short addrRef, unsigned char* ram, VMem* vmem) {
    if (addrRef<VMEM_START_ADDR) {
        unsigned char b2 =  ram[addrRef];
        unsigned char b1 =  ram[addrRef+1];
        return (b2<<8)+b1;
    } else {
        unsigned char b2 =  vread( addrRef, ram,  vmem);
        unsigned char b1 =  vread( addrRef+1, ram,  vmem);
        return (b2<<8)+b1;
    }

}

void vwrite2(unsigned short addrRef,unsigned short val, unsigned char* ram, VMem* vmem) {

    if (addrRef<VMEM_START_ADDR) {
        ram[addrRef] = val >> 8;
        ram[addrRef+1] = val & 0xff;
    } else {
        vwrite(addrRef,val >> 8, ram, vmem);
        vwrite(addrRef+1,val & 0xff, ram, vmem);
    }
}

#else

unsigned char vread(unsigned char ref, unsigned short addrRef, unsigned char* ram, VMem* vmem) {
#ifdef DBG
    printf("vread\n");
#endif

    unsigned char startIdx = vmem->allocMap[ref].startIdx;
    /* we don't check if the address is in range */
    /* unsigned char npages = vmem->allocMap[allocRef].length;  */

    unsigned short refOffset = addrRef >> VMEM_PAGE_BITS;
    unsigned short inPageIdx = addrRef & VMEM_PAGE_MASK;

    unsigned short pageIdx = vmem->usedPages[startIdx+refOffset];
    unsigned short addr = (pageIdx<<VMEM_PAGE_BITS)+inPageIdx;
    return ram[addr];

}

void vwrite(unsigned char ref, unsigned short addrRef,unsigned char val, unsigned char* ram, VMem* vmem) {
#ifdef DBG
    printf("vwrite\n");
#endif
    unsigned char startIdx = vmem->allocMap[ref].startIdx;

    unsigned short refOffset = addrRef >>VMEM_PAGE_BITS;
    unsigned short inPageIdx = addrRef & VMEM_PAGE_MASK;

    unsigned short pageIdx = vmem->usedPages[startIdx+refOffset];
    unsigned short addr = (pageIdx<<VMEM_PAGE_BITS)+inPageIdx;
    ram[addr] = val;
}


unsigned short vread2(unsigned char ref, unsigned short addrRef, unsigned char* ram, VMem* vmem) {
        unsigned char b2 =  vread( ref, addrRef, ram,  vmem);
        unsigned char b1 =  vread( ref, addrRef+1, ram,  vmem);
        return (b2<<8)+b1;
}

void vwrite2(unsigned char ref, unsigned short addrRef,unsigned short val, unsigned char* ram, VMem* vmem) {

    vwrite(ref,addrRef,val >> 8, ram, vmem);
    vwrite(ref,addrRef+1,val & 0xff, ram, vmem);
}
#endif
